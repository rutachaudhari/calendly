# Calendly

Calendar Slot Booking service, which allows people to define their available slots 
on a day and other people to book them. The new user needs to be on-boarded by sigining 
in to be saved on database. The user then needs to login to application and generate token. 
It is then possible for logged in user to define his/her free slots. Another user can then 
book available free slot for a particular user by logging in. 


## Getting Started

Setup server

```
git clone https://gitlab.com/rutachaudhari/calendly.git

```
Import calendly as a maven project in IntelliJ Idea.(You can use your preferred IDE as well). Open MainApplication.java file and run it.

### Prerequisites

```
git
IDE

```
## RESTful APIs

###  1. POST /signup 
```
curl --location --request POST 'http://calendly.rutachaudhari.tech/user/signup' \
--header 'Content-Type: application/json' \
--header 'Cookie: BCSI-CS-4939c139ab107519=1; BCSI-CS-b753c642d0100efc=1' \
--data-raw '{
	"email": "ruta@gmail.com",
	"password": "password"
}'
```
Response body:
```
{
    "timeStamp": 1586159586203,
    "metaData": {
        "message": "User registered"
    }
}
```
###  2. POST /login
```
curl --location --request POST 'http://calendly.rutachaudhari.tech/user/login' \
--header 'Content-Type: application/json' \
--header 'Cookie: BCSI-CS-4939c139ab107519=1; BCSI-CS-b753c642d0100efc=1' \
--data-raw '{
	"email": "ruta@gmail.com",
	"password": "password"
}'
```
Response body:
```
{
    "timeStamp": 1586159635016,
    "metaData": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjYWxlbmRseSIsImV4cCI6MTU4NjE2MzIzNSwianRpIjoiMGZkMTUzZWUtNTgxNi00OWU0LTgwNWYtZTQ1OTJlOTI0ODIxIiwiZW1haWwiOiJydXRhQGdtYWlsLmNvbSJ9.73idrAKq6EKKk4XdJ_8bhNlUji6Rlrx5IpNkv1d0cPc"
    }
}
```
###  3. POST /freeSlots
```
curl --location --request POST 'http://calendly.rutachaudhari.tech/user/freeSlots' \
--header 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjYWxlbmRseSIsImV4cCI6MTU4NjE2MzIzNSwianRpIjoiMGZkMTUzZWUtNTgxNi00OWU0LTgwNWYtZTQ1OTJlOTI0ODIxIiwiZW1haWwiOiJydXRhQGdtYWlsLmNvbSJ9.73idrAKq6EKKk4XdJ_8bhNlUji6Rlrx5IpNkv1d0cPc' \
--header 'Content-Type: application/json' \
--header 'Cookie: BCSI-CS-4939c139ab107519=1; BCSI-CS-b753c642d0100efc=1' \
--data-raw '{
	"slots" : [14, 20, 10, 13, 9]
}'
```
Response body:
```
{
    "timeStamp": 1586159715796,
    "metaData": {
        "message": "user slots registered"
    }
}
```

###  4. POST /bookSlots

We need two users for this. The logged in user can specify the email of another user to book slot.
```
curl --location --request POST 'http://calendly.rutachaudhari.tech/user/bookSlots' \
--header 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjYWxlbmRseSIsImV4cCI6MTU4NjE2MzIzNSwianRpIjoiMGZkMTUzZWUtNTgxNi00OWU0LTgwNWYtZTQ1OTJlOTI0ODIxIiwiZW1haWwiOiJydXRhQGdtYWlsLmNvbSJ9.73idrAKq6EKKk4XdJ_8bhNlUji6Rlrx5IpNkv1d0cPc' \
--header 'Content-Type: application/json' \
--header 'Cookie: BCSI-CS-4939c139ab107519=1; BCSI-CS-b753c642d0100efc=1' \
--data-raw '{
	"email": "john@gmail.com",
	 "slot": 14
}'
```
Response body:
```
{
    "timeStamp": 1586159758783,
    "metaData": {
        "message": "Your slot is booked!"
    }
}
```
###  5. GET /mySchedule

We need two users for this. The logged in user can specify the email of another user to book slot.
```
curl --location --request GET 'http://calendly.rutachaudhari.tech/user/mySchedule' \
--header 'token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjYWxlbmRseSIsImV4cCI6MTU4NjE2MzA2MywianRpIjoiZDdiYmNiMWYtMDE5Ny00NjViLTg5Y2YtMmM3YmJmNGNhZWM0IiwiZW1haWwiOiJqb2huQGdtYWlsLmNvbSJ9.SHC_0trYlF86oZt5OThBgXnMZcu_sCVyVyGGmeBCuyQ' \
--header 'Cookie: BCSI-CS-4939c139ab107519=1; BCSI-CS-b753c642d0100efc=1'
```
Response body:
```
{
    "timeStamp": 1586159801845,
    "metaData": {
        "schedule": {
            "11": "FREE",
            "12": "FREE",
            "13": "FREE",
            "14": "ruta@gmail.com",
            "15": "FREE",
            "16": "FREE",
            "17": "FREE",
            "18": "FREE",
            "19": "FREE",
            "9": "FREE",
            "10": "FREE"
        }
    }
}
```
## Author

* **Ruta Chaudhari** 


