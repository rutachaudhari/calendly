package com.ruta.calendly.calendly.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public enum ErrorCode {


    UNAUTHORIZED_TOKEN("ERR_01" , "TOKEN EXPIRED OR TEMPERED" , HttpStatus.UNAUTHORIZED),
    UNHANDLED_EXCEPTION("ERR_02" , "EXCEPTION UNHANDLED" , HttpStatus.INTERNAL_SERVER_ERROR),
    CREDENTIAL_MISMATCH("ERR_03" , "INVALID EMAIL/PASSWORD", HttpStatus.UNAUTHORIZED),
    BAD_REQUEST("ERR_04", "Bad request" , HttpStatus.BAD_REQUEST),
    USER_NOT_FOUND("ERR_05", "User for entered email not found" , HttpStatus.NOT_FOUND),
    TIME_SLOT_NOT_FOUND("ERR_06" , "Time slot for email not found", HttpStatus.NOT_FOUND),
    TIME_SLOT_NOT_AVAILABLE("ERR_06" , "Time slot for email not available", HttpStatus.NOT_FOUND),
    USER_ALREADY_EXISTS("ERR_07", "User already exists" , HttpStatus.BAD_REQUEST);
    private final String errorCode;
    private final String errorMessage;
    private final HttpStatus httpStatus;
}
