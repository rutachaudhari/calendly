package com.ruta.calendly.calendly.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserBookSlotsRequestDto {

    private String email;
    private Integer slot;
}
