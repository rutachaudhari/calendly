package com.ruta.calendly.calendly.Dto;


import lombok.Getter;

@Getter
public class UserLoginRequestDto {

    private String email;
    private String password;
}
