package com.ruta.calendly.calendly.Dto;

import lombok.Getter;

import java.util.ArrayList;


@Getter
public class UserSlotsRequestDto {


    private ArrayList<Integer> slots;
}
