package com.ruta.calendly.calendly.Exceptions;

import com.ruta.calendly.calendly.Enums.ErrorCode;

public class NotFoundException extends AbstractUserException {
    public NotFoundException(ErrorCode errorCode) {
        super(errorCode);
    }
}
